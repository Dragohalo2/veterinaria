
from django.contrib import admin
from django.urls import path
from PanelAdmin.views.productosViews import ProductosView, ProductosListView,ProductosRUDV

urlpatterns = [
    path('admin/', admin.site.urls),
    path('productos/',ProductosView.as_view()),
    path('productos_list/',ProductosListView.as_view()),
    path('productos_rudv/<int:pk>/',ProductosRUDV.as_view())
]
