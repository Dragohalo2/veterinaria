from django.db import models


class CompraServicios(models.Model):
 id=models.AutoField(primary_key=True)
 servicio=models.CharField(max_length=30)
 tiposervicio=models.CharField(max_length=30) 
 descripcion=models.CharField(max_length=100)
 profesional=models.CharField(max_length=20)
 