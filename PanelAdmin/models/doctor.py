from django.db import models

class doctordis(models.Model):
    id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    cargo = models.CharField(max_length=100, null=True)
    disponibilidad = models.CharField(max_length=80)
    contacto = models.CharField(max_length=10)