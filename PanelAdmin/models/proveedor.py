from django.db import models

class Provedor(models.Model):
    id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    direction = models.CharField(max_length=100, null=True)
    email = models.CharField(max_length=80)
    cellphone = models.CharField(max_length=10)