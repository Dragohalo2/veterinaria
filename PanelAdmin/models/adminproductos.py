from django.db import models

class AdminProductos(models.Model): 
    id = models.AutoField(primary_key=True)
    producto = 	models.CharField(max_length=20)
    precio = models.FloatField()
    marca = models.CharField(max_length=50)
    fecha = models.DateTimeField(null = True)
    disposicion = models.BooleanField()
    disponibles = models.IntegerField()