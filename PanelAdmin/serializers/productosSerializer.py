from rest_framework import serializers
from PanelAdmin.models.adminproductos import AdminProductos

class ProductosSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdminProductos
        fields = ['id','producto','precio', 'marca','disposicion','disponibles']