from rest_framework import serializers
from PanelAdmin.models.compraServicios import CompraServicios

class CompraServiciosSerializer(serializers.ModelSerializer):
    class Meta:
        model = CompraServicios
        fields = ['id','servicio','tiposervicio']