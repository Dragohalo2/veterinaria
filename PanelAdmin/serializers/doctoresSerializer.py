from rest_framework import serializers
from PanelAdmin.models.doctor import doctordis

class DoctoresSerializer(serializers.ModelSerializer):
    class Meta:
        model = doctordis
        fields = ['cargo','disponibilidad']