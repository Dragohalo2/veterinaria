# Generated by Django 3.2.8 on 2021-10-05 21:54

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Provedor',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('first_name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=50)),
                ('direction', models.CharField(max_length=100, null=True)),
                ('email', models.CharField(max_length=80)),
                ('cellphone', models.CharField(max_length=10)),
            ],
        ),
    ]
