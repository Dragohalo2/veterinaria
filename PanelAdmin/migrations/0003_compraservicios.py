# Generated by Django 3.2.8 on 2021-10-08 23:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('PanelAdmin', '0002_adminproductos'),
    ]

    operations = [
        migrations.CreateModel(
            name='CompraServicios',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('servicio', models.CharField(max_length=30)),
                ('tiposervicio', models.CharField(max_length=30)),
                ('descripcion', models.CharField(max_length=100)),
                ('profesional', models.CharField(max_length=20)),
            ],
        ),
    ]
