from rest_framework import views, status
from rest_framework.response import Response
from PanelAdmin.models import CompraServicios
from PanelAdmin.serializers import CompraServiciosSerializer
from rest_framework import generics



class CompraServiciosView(views.APIView):
    def get(self, request):
        queryset = CompraServicios.objects.all() 
        serialized = CompraServiciosSerializer(queryset, many=True)
        return Response(serialized.data, status=status.HTTP_200_OK)
        

class CompraServiciosCreateView(generics.ListCreateAPIView):
    queryset = CompraServicios.objects.all() 
    serializer_class=CompraServiciosSerializer
    

