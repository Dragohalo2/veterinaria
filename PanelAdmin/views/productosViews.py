from django.db.models.query import QuerySet
from PanelAdmin import serializers
from rest_framework import views, status
from rest_framework.response import Response
from PanelAdmin.models import AdminProductos
from PanelAdmin.serializers import ProductosSerializer
from rest_framework import generics

class ProductosView(views.APIView):

    def get(self, request):
        queryset = AdminProductos.objects.all() #obtiene los objetos de la base de datos
        serializador = ProductosSerializer(queryset,many=True)  #Convierte objetos de la base de datos a JSON
        return Response(serializador.data, status=status.HTTP_200_OK)
        
#Listar todas las cuentas y crear una cuenta
class ProductosListView(generics.ListCreateAPIView):
        queryset = AdminProductos.objects.all()   
        serializer_class = ProductosSerializer

#retreve, update, delete a account
class ProductosRUDV(generics.RetrieveUpdateDestroyAPIView):
     queryset = AdminProductos.objects.all()
     serializer_class = ProductosSerializer
